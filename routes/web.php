<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ImageInfo;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', function () {
    return view('admin.dashboard');
});

// Route::get('/admin','ImageInfo@list' );
Route::get('admin', [ImageInfo::class, 'disp'],function(){
    return view('admin.dashboard');
});

Route::get('/deleteimage/{id}',[ImageInfo::class, 'delete']);

Route::get('/editimage/{id}',[ImageInfo::class, 'edit'],function(){
    return view('admin.imageupdateform');
});

Route::put('/updateimage/{id}',[ImageInfo::class, 'update']);

// Route::get('/allimages',[ImageInfo::class, 'dispimg'],function(){
//     return view('admin.imageapi');
// });