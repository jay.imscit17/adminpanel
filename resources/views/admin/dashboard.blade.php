@extends('layouts.master')

@section('title')
	Admin Panel
@endsection

@section('content')

<div class="row">
@foreach($data as $d)
<div class="col-lg-3 col-md-6 col-sm-12 my-2">
  <div class="card shadow h-100 py-2 col-md-12">
    <div class="row">
      <div class="col-md-12 justify-content-center">
	  <p>{{$d['id']}}</p>
          <img class="card-img-top" src="{{ asset('../assets/img/db_images/'.$d['image'] ) }}" alt="">
<!--           <img class="card-img-top" src="{{ asset('http://192.168.1.102/Android_db/db_images/'.$d['image'] ) }}" alt=""> -->
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 my-2">
	  
        <h5>{{$d['name']}}</h5>
          <center>
		  <a href="/editimage/{{$d['id']}}" class="btn btn-primary">Update</a>
		<a href="/deleteimage/{{$d['id']}}" class="btn btn-primary">Delete</a></center>
      </div>
    </div>
  </div>
</div>
@endforeach


@endsection

@section('scripts')

@endsection