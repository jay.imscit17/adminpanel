@extends('layouts.master')

@section('title')
	Admin Panel
@endsection

@section('content')

<div class="container">
        <div class="row">
            <div class="col-md-6 mt-5">
                <form method="POST" action="/updateimage/{{$updateData->id}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    {{ method_field("PUT")}}
                    <input type="hidden" name="id" id="id" value="{{ $updateData->id }}">
                    <div class="form-group row">
                    <label for="Name" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                    <input type="text" name="name" class="form-control" id="name"  value="{{ $updateData->name }}">
                    </div></div>
                    <img id="output"  style="width: 100%; height: 100%;" src="{{ asset('../assets/img/db_images/'.$updateData->image ) }}"/>
                    <div class="form-group">
                        <label for="file-input" class="btn btn-default" flow-btn>Please choose a file
                        <input id="file-input" type="file" name="image" style="visibility: hidden; position: absolute;" value="{{ $updateData->image }}" onchange="loadFile(event)">
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-warning">Update</button>
                    </div>

                </form>
            </div>
        </div>
    </div>



@endsection

@section('scripts')

@endsection