<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\http;
use App\Models\Captured_imgsinfo;
use Image;
use Storage;
class ImageInfo extends Controller
{
    //
    
    public function insertapi(Request $req)
    {
        $insertdata=new Captured_imgsinfo();
        $insertdata->name=$req->input('name');
            $img=$req->upload;
            $filename= "IMG".rand().".jpg";           
        Image::make($img)->save(file_put_contents('assets/img/db_images/'.$filename,base64_decode($img)));
            $insertdata->image=$filename;
        if($insertdata->save()){
            return response()->json(['response'=>'Image Uploaded Successfully'])->withCallback($req->input("response"));
        }
        else{
            return response()->json(['response'=>'Image Upload Failed'])->withCallback($req->input("response"));
        }        
    }

    function disp()
    {
        $data=Captured_imgsinfo::all();
        return view('admin.dashboard',['data'=>$data]);
    }
    
    public function listapi()
    {
        $disp = Captured_imgsinfo::all();
        return response()->json($disp);
    }

    public function edit($id)
    {
        $imgdata = Captured_imgsinfo::find($id);
        return view('admin.imageupdateform')->with('updateData',$imgdata);
    }

    public function update(Request $req,$id)
    {
        $imgdata=Captured_imgsinfo::find($id);
        $imgdata->name=$req->input('name');
        if($req->hasfile('image'))
        {
        $img=$req->file('image');
        $filename= "IMG".rand().".jpg";           
        Image::make($img)->save($img->move('assets/img/db_images/',$filename));
        $imgdata->image=$filename;
        }$imgdata->save();

        return redirect('/admin')->with('updateData',$imgdata);
    }



    public function delete($id)
    {
        $deleteimg=Captured_imgsinfo::find($id);
        $deleteimg->delete();
        return redirect('/admin')->with('deleteimg',$deleteimg);
    }

    public function dispimg($imagname)
    {
        $url=Storage::url('../assets/img/db_images/'.$imagname);
        return "<img src='".$url."'/ width='224' height='400'>";

    }
    
}
