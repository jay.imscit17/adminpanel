<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Captured_imgsinfo extends Model
{
    use HasFactory;
    protected $table = 'captured_imgsinfo';
    protected $dispdata = ['id','name','image'];
    public $timestamps=false;
}
